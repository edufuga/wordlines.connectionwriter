package com.gitlab.edufuga.wordlines.connectionwriter;

import com.gitlab.edufuga.wordlines.recordreader.RecordReader;
import com.gitlab.edufuga.wordlines.recordreader.FileSystemRecordReader;
import com.gitlab.edufuga.wordlines.core.Status;
import com.gitlab.edufuga.wordlines.core.WordStatusDate;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ConnectionWriter {
    private final String connectionFolder;
    private final Map<String, RecordReader> fromRecordReaders = new HashMap<>();
    private final Map<String, RecordReader> toRecordReaders = new HashMap<>();
    private final Map<String, RecordReader> coRecordReaders = new HashMap<>();

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public ConnectionWriter(String connectionFolder) {
        this.connectionFolder = connectionFolder;
    }

    public void write(String fromStatusFolder, String fromWord,
                      String toStatusFolder, String toWord,
                      String coStatusFolder, String coWord) throws Exception {
        RecordReader fromReader = fromRecordReaders.get(fromStatusFolder);
        if (fromReader == null) {
            fromReader = new FileSystemRecordReader(fromStatusFolder);
            fromRecordReaders.put(fromStatusFolder, fromReader);
        }


        RecordReader toReader = toRecordReaders.get(toStatusFolder);
        if (toReader == null) {
            toReader = new FileSystemRecordReader(toStatusFolder);
            toRecordReaders.put(toStatusFolder, toReader);
        }


        RecordReader coReader = coRecordReaders.get(coStatusFolder);
        if (coReader == null) {
            coReader = new FileSystemRecordReader(coStatusFolder);
            coRecordReaders.put(coStatusFolder, coReader);
        }


        WordStatusDate from = fromReader.readRecord(fromWord);
        WordStatusDate to = toReader.readRecord(toWord);
        WordStatusDate co = coReader.readRecord(coWord);

        if (from == null || from.getStatus() == null) {
            throw new Exception();
        }


        if (to == null || to.getStatus() == null) {
            throw new Exception();
        }


        if (co == null || co.getStatus() == null) {
            throw new Exception();
        }


        if (from.getStatus().equals(Status.IGNORED)) {
            throw new Exception();
        }


        if (to.getStatus().equals(Status.IGNORED)) {
            throw new Exception();
        }


        if (co.getStatus().equals(Status.IGNORED)) {
            throw new Exception();
        }


        if (from.getStatus().equals(Status.UNKNOWN)) {
            throw new Exception();
        }


        if (to.getStatus().equals(Status.UNKNOWN)) {
            throw new Exception();
        }


        if (co.getStatus().equals(Status.UNKNOWN)) {
            throw new Exception();
        }


        // Write a new connection file to the corresponding path.
        // [connection root folder]/DE/Haus/CAT/casa/$filename

        String fromLanguage = Paths.get(fromStatusFolder).getFileName().toString();
        String toLanguage = Paths.get(toStatusFolder).getFileName().toString();
        String coLanguage = Paths.get(coStatusFolder).getFileName().toString();

        // Nested file structure.
        Path destinationFolder = Paths.get(connectionFolder)
                .resolve(fromLanguage)
                .resolve(from.getWord())
                .resolve(coLanguage)
                .resolve(co.getWord())
                .resolve(toLanguage)
                .resolve(to.getWord());

        if (Files.notExists(destinationFolder)) {
            Files.createDirectories(destinationFolder);
        }


        Date date = new Date();

        // With a nested folder structure there is no real need to save the TSV file, but I like to have it explicitly.
        // It doesn't hurt to have a physical file (TSV) with the information in one place (i.e. flat file; no nesting).
        // Furthermore the date is also saved.
        String filename = "" + fromLanguage + "_" + from.getWord() + "_" + coLanguage + "_" + co.getWord() + "_"
                + toLanguage + "_" + to.getWord() + ".tsv";
        Path destination = destinationFolder.resolve(filename);
        File file = destination.toFile();

        String result = "" + fromLanguage + "\t" + from.getWord() + "\t" + coLanguage + "\t" + co.getWord() + "\t"
                + toLanguage + "\t" + to.getWord() + "\t" + dateFormat.format(date) + "\n";

        Files.write(file.toPath(), result.getBytes());
    }

    public static void main(String[] args) {
        System.out.println("This is the ConnectionWriter. It still doesn't do anything here.");
    }
}
